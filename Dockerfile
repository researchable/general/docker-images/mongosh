FROM debian:latest
LABEL maintainer="Researchable <docker@researchable.nl>"

WORKDIR /app

RUN apt-get update && \
    apt-get install -y wget libgssapi-krb5-2 && \
    wget -O mongosh.deb https://downloads.mongodb.com/compass/mongodb-mongosh_1.0.4_amd64.deb && \
    dpkg -i mongosh.deb

ENTRYPOINT ["mongosh"]
