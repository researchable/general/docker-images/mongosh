# 1.0.0 (2021-12-08)


### Bug Fixes

* added  post-deploy stage ([5d7f6ad](https://gitlab.com/researchable/general/docker-images/mongosh/commit/5d7f6ad3c29860e5089a30eeb09a5bd3429ff08c))
* added test_registry_job stage ([0ff731f](https://gitlab.com/researchable/general/docker-images/mongosh/commit/0ff731fb13f64e767d201a154830487807fc0a51))


### Features

* Added mongosh in docker ([f38061b](https://gitlab.com/researchable/general/docker-images/mongosh/commit/f38061b495d41d30ac64e27fdf85e5e5144a418f))
